function makePagination(totalItems, currentPage, pageSize) {

}

// { currentPage: 3, totalPages: 20, pages: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10] }
console.log(makePagination(200, 3, 10))

// { currentPage: 10, totalPages: 101, pages: [1, 2, 3, 4, 5, ..., 101] } where "..." is a number from 5 to 101
console.log(makePagination(2007, 10, 20))
